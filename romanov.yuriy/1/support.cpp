#include <stdexcept>
#include <cstring>
#include "support.h"

bool isAscending(const char *str)
{
  if (!strcmp(str, "ascending")) {
    return true;
  } else {
    if (!strcmp(str, "descending")) {
      return false;
    }
  }
  throw std::invalid_argument("Wrong direction");
}

