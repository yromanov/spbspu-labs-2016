#include "process_2.h"
#include <cstring>
#include <iostream>
#include <vector>
#include <bits/unique_ptr.h>
#include <iterator>

void process_2(std::ifstream &source) throw(std::string)
{
  size_t size = 50, i = 0;
  char *mas = (char*) malloc(size*sizeof(char));
  while (!source.eof()) {
    if ((i + 1) == size) {
      size += 50;
      char *masTmp = (char *) realloc(mas, size);
      if (masTmp != nullptr) {
          mas=masTmp;
      } else {
        free(mas);
        throw std::runtime_error("Process_2:Not enough memory.");
      }
    }
    mas[i] = (char) source.get();
    if ((source.fail() || source.bad()) && (!source.eof())) {
      free(mas);
      throw std::runtime_error("Process_2:File error.");
    }
    i++;
  }
  
  std::vector<char> vec(mas, mas + (i - 1) * sizeof(char));
  free(mas);
  std::copy(vec.begin(),vec.end(), std::ostream_iterator<char>(std::cout));
}
