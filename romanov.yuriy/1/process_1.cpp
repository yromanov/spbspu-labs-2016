#include "process_1.h"
#include "sorter.h"
#include "support.h"
#include <vector>
#include <stdexcept>
#include <iostream>
#include <forward_list>

void process_1(const bool direction)
{
  std::vector<int> vec(std::istream_iterator<int>(std::cin),std::istream_iterator<int>());
  if (!std::cin && !std::cin.eof()) {
    throw std::runtime_error("Process_1:Not a number.");
  }
  
  std::vector<int> vecTmp = vec;
  Sorter<std::vector<int>> *sorter1 = new Sorter<std::vector<int>>(new FirstSorting<std::vector<int>>);
  sorter1->sortContainer(vecTmp, direction);
  delete (sorter1);
  display<std::vector<int>>(vec);
  
  vecTmp = vec;
  Sorter<std::vector<int>> *sorter2 = new Sorter<std::vector<int>>(new SecondSorting<std::vector<int>>);
  sorter2->sortContainer(vecTmp, direction);
  delete (sorter2);
  display<std::vector<int>>(vec);
  
  std::forward_list<int> list;
  for (int i = signed(vec.size() - 1); i >= 0; i--) {
    list.push_front(vec[i]);
  }
  Sorter<std::forward_list<int>> *sorter3 = new Sorter<std::forward_list<int>>(
          new ThirdSorting<std::forward_list<int>>);
  sorter3->sortContainer(list, direction);
  delete (sorter3);
  display<std::forward_list<int>>(list);
}

