#ifndef LAB_1_SORTER_H
#define LAB_1_SORTER_H

#include<utility>

template<class TemplContainer>
class Sorting {
public:
  virtual void sortContainer(TemplContainer &cont, const bool direction) = 0;
  virtual ~Sorting(){};
};

template<class TemplContainer>
class FirstSorting : public Sorting<TemplContainer> {
  void sortContainer(TemplContainer &cont, const bool direction)
  {
    for (int i = 0; i < signed(cont.size()); i++) {
      for (int j = 0; j < signed(cont.size()) - i - 1; j++) {
        if ((cont[j] < cont[j + 1]) ^ direction) {
          std::swap(cont[j], cont[j + 1]);
        }
      }
    }
  }
};

template<class TemplContainer>
class SecondSorting : public Sorting<TemplContainer> {
  void sortContainer(TemplContainer &cont, const bool direction)
  {
    for (int i = 0; i < signed(cont.size()); i++) {
      for (int j = 0; j < signed(cont.size()) - i - 1; j++) {
        if ((cont[j] < cont[j + 1]) ^ direction) {
          std::swap(cont.at(j), cont.at(j + 1));
        }
      }
    }
  }
};

template<class TemplContainer>
class ThirdSorting : public Sorting<TemplContainer> {
  void sortContainer(TemplContainer &cont, const bool direction)
  {
    typename TemplContainer::iterator iteri = cont.begin();
    typename TemplContainer::iterator iterj = iteri;
    while (iteri != cont.end()) {
      while (iterj != cont.end()) {
        if ((*iteri < *iterj) ^ direction) {
          std::swap(*iteri, *iterj);
        }
        iterj++;
      }
      iteri++;
      iterj = iteri;
    }
  }
};

template<class TemplContainer>
class Sorter {
public:
  Sorter(Sorting<TemplContainer> *p) : sorter_(p)
  {}
  
  ~Sorter()
  {
    delete sorter_;
  }
  
  void sortContainer(TemplContainer &container, const bool direction)
  {
    sorter_->sortContainer(container, direction);
  }

private:
  Sorting<TemplContainer> *sorter_;
};

#endif
