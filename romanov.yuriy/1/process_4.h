#ifndef LAB_1_PROCESS_4
#define LAB_1_PROCESS_4

#include <cstddef>

void process_4(const bool direction, const size_t size);

#endif
