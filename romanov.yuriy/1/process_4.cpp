#include <vector>
#include <ctime>
#include "process_4.h"
#include "support.h"
#include "sorter.h"


void fillRandom(double *array, const size_t size)
{
  srand(time(0));
  for (size_t i = 0; i < size; i++) {
    array[i] = (double) (rand()%200000-100000)/(double)100000;
  }
}

void process_4(const bool direction, const size_t size)
{
  std::vector<double> vec(size);
  fillRandom(&vec[0], size);
  display<std::vector<double>>(vec);
  Sorter<std::vector<double>> *sorter1 = new Sorter<std::vector<double>>(new FirstSorting<std::vector<double>>);
  sorter1->sortContainer(vec, direction);
  delete (sorter1);
  display<std::vector<double>>(vec);
}
