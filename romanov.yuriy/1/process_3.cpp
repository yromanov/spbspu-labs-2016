#include "process_3.h"
#include "support.h"
#include <vector>
#include <algorithm>

#define INSERT_VALUE 1
#define INSERT_COUNT 3

void process_3() throw(std::string)
{
  std::vector<int> vec(std::istream_iterator<int>(std::cin),std::istream_iterator<int>());
  if (!std::cin && !std::cin.eof()) {
    throw std::runtime_error("Process_3:Cin Error.");
  }
  
  if (!vec.empty()) {
    switch (vec.back()) {
    case 1: {
      vec.erase(std::remove_if(vec.begin(), vec.end(), [](int i) {
        return i%2 == 0;
      }), vec.end());
      break;
    }
    case 2: {
      std::vector<int>::iterator iter = vec.begin();
      while (iter != vec.end()) {
        if (*iter % 3 == 0) {
          iter = vec.insert(++iter, INSERT_COUNT, INSERT_VALUE);
          iter += 2;
        } else {
          iter++;
        }
      }
      break;
    }
    }
  }
  display<std::vector<int>>(vec);
}
