#ifndef LAB_1_OUTPUT_H
#define LAB_1_OUTPUT_H

#include <iostream>
#include <iterator>

bool isAscending(const char *str);

template<typename ContainerType>
void display(ContainerType cont)
{
  if (cont.empty()) {
    std::cout << "Empty.\n";
  } else {
    std::copy(cont.begin(),cont.end(), std::ostream_iterator<typename ContainerType::value_type>(std::cout, " "));
    std::cout << '\n';
  }
}

#endif
