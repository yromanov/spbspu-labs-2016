#ifndef LAB_1_PROCESS_2
#define LAB_1_PROCESS_2

#include <string>
#include <fstream>

void process_2(std::ifstream &source) throw(std::string);

#endif
