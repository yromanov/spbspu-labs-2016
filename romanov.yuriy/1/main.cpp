#include <iostream>
#include <stdexcept>
#include "process_1.h"
#include "process_2.h"
#include "process_3.h"
#include "process_4.h"
#include "support.h"


int main(int argc, char *argv[])
{
  try {
    if ((argc > 4) || (argc < 2)) {
      throw std::invalid_argument("Too many or not enough arguments.");
    }
    switch (atoi(argv[1])) {
    case 1: {
      if (argc == 3) {
        process_1(isAscending(argv[2]));
      } else {
        throw std::invalid_argument("Option 1 takes 1 argument.");
      }
      break;
    }
    case 2: {
      if (argc == 3) {
        std::ifstream source(argv[2], std::ios::in);
        if (source.is_open()) {
          if (source.peek() != std::ifstream::traits_type::eof()) {
            process_2(source);
          }
          source.close();
        } else {
          throw std::invalid_argument("Wrong file name.");
        }
      } else {
        throw std::invalid_argument("Option 2 takes 2 arguments.");
      }
      break;
    }
    case 3: {
      if (argc == 2) {
        process_3();
      } else {
        throw std::invalid_argument("Option 3 do not take arguments.");
      }
      break;
    }
    case 4: {
      if (argc != 4) {
        throw std::invalid_argument("Option 4 takes 2 arguments.");
      }
      const size_t tmp = (size_t) atoi(argv[3]);
      if (((tmp == 0) && (*argv[3] != '0')) || (tmp < 1)) {
        throw std::invalid_argument("Wrong number.");
      }
      process_4(isAscending(argv[2]), tmp);
      break;
    }
    default: {
      throw 0;
    }
    }
  }
  catch (const std::invalid_argument &er) {
    std::cerr << er.what() << '\n';
  }
  catch (const std::runtime_error &er) {
    std::cerr << er.what() << '\n';
  }
  catch (...) {
    std::cerr << "Something wrong.\n";
    return 2;
  }
}
