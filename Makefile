.PHONY: all labs clean
.SECONDEXPANSION:
.SECONDARY:

BOOST_LOCATION := $(shell test -f .boost_location && cat .boost_location ; true)

CPPFLAGS += -Wall -Wextra -Werror $(if $(BOOST_LOCATION),-I$(BOOST_LOCATION))
CXXFLAGS += -g

system   := $(shell uname)

ifneq 'MINGW' '$(patsubst MINGW%,MINGW,$(system))'
CPPFLAGS += -std=c++11
else
CPPFLAGS += -std=gnu++11
endif

ifeq 'Darwin' '$(system)'
TIMEOUT_CMD := gtimeout
else
TIMEOUT_CMD := timeout
endif

students := $(filter-out out Makefile README.md,$(wildcard *))
labs     := $(foreach student,$(students),$(wildcard $(student)/*))

lab_test_sources = $(filter $(1)/test-%.cpp,$(wildcard $(1)/*.cpp))
lab_sources      = $(filter-out $(1)/test-%,$(wildcard $(1)/*.cpp))

lab_objects      = $(patsubst %.cpp,out/%.o,$(call lab_sources,$(1)))
lab_test_objects = $(patsubst %.cpp,out/%.o,$(call lab_test_sources,$(1)))

objects          := $(foreach lab,$(labs),$(call lab_objects,$(lab)))
test_objects     := $(foreach lab,$(labs),$(call lab_test_objects,$(lab)))

all: $(addprefix build-,$(lab_targets))

labs:
	@echo $(labs)

$(addprefix run-,$(labs)): run-%: out/%/lab
	@$(if $(TIMEOUT),$(TIMEOUT_CMD) $(TIMEOUT)s )$(if $(VALGRIND),valgrind $(VALGRIND) )$< $(ARGS)

clean:
	rm -rf out

$(addprefix build-,$(labs)): build-%: out/%/lab

$(addprefix test-,$(labs)): test-%: out/%/test-lab
	@$(if $(TIMEOUT),$(TIMEOUT_CMD) $(TIMEOUT)s )$(if $(VALGRIND),valgrind $(VALGRIND) )$< $(TEST_ARGS)

out/%/lab: $$(call lab_objects,%) | $$(@D)/.dir
	@$(CXX) $(CPPFLAGS) $(CXXFLAGS) $(LDFLAGS) -o $@ $^

out/%/test-lab: $$(call lab_test_objects,%) $$(call lab_objects,%) | $$(@D)/.dir
	@$(CXX) $(CPPFLAGS) $(CXXFLAGS) $(LDFLAGS) -o $@ $(filter-out %/main.o,$^)

$(test_objects): out/%.o: %.cpp | $$(@D)/.dir
	@$(CXX) $(CPPFLAGS) $(CXXFLAGS) -MMD -MP -c -Wno-unused-parameter -o $@ $<

$(objects): out/%.o: %.cpp | $$(@D)/.dir
	@$(CXX) $(CPPFLAGS) $(CXXFLAGS) -MMD -MP -c -o $@ $<

%/.dir:
	@mkdir -p $(@D) && touch $@

include $(wildcard $(patsubst %.o,%.d,$(objects)))
